package com.example.exam;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RationRepo extends JpaRepository<RationShop, Long> {

public RationShop findById(long id);
public List<RationShop> findByState(String state);

public void deleteById(long id);
@Query("select sum(revenueModel) from RationShop where state=:state")
public Long findRevenueByState(String state);

}
