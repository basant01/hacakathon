package com.example.exam;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class RationController {


@Autowired
RationRepo rationRepo;

@GetMapping("/api/ration")
public List<RationShop> listAllRationShops()
{
	return rationRepo.findAll();
}

@PostMapping("/api/ration")
public RationShop createRationShop(@RequestBody RationShop rationShop)
{
	return rationRepo.save(rationShop);
	
}

@Transactional
@DeleteMapping("/api/ration/{id}")
public void deleteRationShop(@PathVariable long id)
{
	rationRepo.deleteById(id);
}
@PutMapping("/api/ration/{id}")
public RationShop updateRationShop(@PathVariable long id, @RequestBody RationShop updatedRationShop)
{
	RationShop rationShop = this.rationRepo.findById(id);
	rationShop.setName(updatedRationShop.getName());
	rationShop.setAddress(updatedRationShop.getAddress());
	rationShop.setCity(updatedRationShop.getCity());
	rationShop.setState(updatedRationShop.getState());
	rationShop.setCountry(updatedRationShop.getCountry());
    rationShop.setRevenueModel(updatedRationShop.getRevenueModel());
	return this.rationRepo.save(rationShop);
	
}
@GetMapping("/api/ration")
public List<RationShop> listRationShopsByState(@RequestParam String state)
{
	return this.rationRepo.findByState(state);
}

@GetMapping("/api/ration/revenue")
public Long getRevenueByState(@RequestParam String state)
{
	return this.rationRepo.findRevenueByState(state);
}

}
